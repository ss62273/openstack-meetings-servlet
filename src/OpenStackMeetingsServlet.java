import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * 
 * CS 378 Modern Web Applications - Assignment 1 OpenStackMeetingsServlet
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

public class OpenStackMeetingsServlet extends HttpServlet {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  // Output sections
  private static final String HISTORY = "History";
  private static final String DATA = "Data";

  // OpenStack Projects Website URL
  private static final String OPEN_STACK_URL = "http://eavesdrop.openstack.org/meetings";

  // ArrayLists storing output for each section
  private static ArrayList<String> history_list;
  private static ArrayList<String> data_list;

  /**
   * 
   * Initialize the Servlet & ArrayLists
   * 
   */
  @Override
  public void init(ServletConfig config) {
    history_list = new ArrayList<String>();
    data_list = new ArrayList<String>();
  }

  /**
   * 
   * Handle user GET request with optional session tracking
   * 
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // URL with query string
    final String QUERY_URL = request.getRequestURL().toString()
        + (request.getQueryString() != null ? "?" + request.getQueryString().toString() : "");
    // Parameter values
    final String PROJECT_VALUE = request.getParameter("project");
    final String YEAR_VALUE = request.getParameter("year");

    // Set response content type
    response.setContentType("text/html");
    PrintWriter pw = response.getWriter();

    // Session parameter value
    String session_value = request.getParameter("session");
    // Get current session if existing, else return null
    HttpSession session = request.getSession(false);

    // Session start/end
    if (session_value != null) {
      if (session_value.equalsIgnoreCase("start")) {
        // Create new session
        session = request.getSession(true);
        // Clear lists
        history_list = new ArrayList<String>();
        data_list = new ArrayList<String>();
        history_list.add(QUERY_URL);
        pw.println(HISTORY + "<br>");
        pw.println("<br>" + DATA);
      }
      if (session_value.equalsIgnoreCase("end")) {
        pw.println(HISTORY);
        for (String url : history_list) {
          pw.println("<br>" + url);
        }
        pw.println("<br><br>" + DATA);
        // Clear lists
        history_list = new ArrayList<String>();
        data_list = new ArrayList<String>();
        // Invalidate current session
        if (session != null) {
          session.invalidate();
        }
      }
      return;
    }

    // No existing session
    if (session == null) {
      history_list = new ArrayList<String>();
      data_list = new ArrayList<String>();
      if (getData(pw, QUERY_URL, PROJECT_VALUE, YEAR_VALUE)) {
        pw.println(HISTORY + "<br>");
        pw.println("<br>" + DATA);
        for (String link : data_list) {
          pw.println("<br>" + link);
        }
      }
      // Clear lists because interaction w/o session
      history_list = new ArrayList<String>();
      data_list = new ArrayList<String>();
    } else if (session.isNew()) { // New session
      if (getData(pw, QUERY_URL, PROJECT_VALUE, YEAR_VALUE)) {
        pw.println(HISTORY);
        for (String url : history_list) {
          pw.println("<br>" + url);
        }
        pw.println("<br><br>" + DATA);
        for (String link : data_list) {
          pw.println("<br>" + link);
        }
      }
    } else { // Continue existing session
      data_list = new ArrayList<String>();
      if (getData(pw, QUERY_URL, PROJECT_VALUE, YEAR_VALUE)) {
        pw.println(HISTORY);
        for (String url : history_list) {
          pw.println("<br>" + url);
        }
        pw.println("<br><br>" + DATA);
        for (String link : data_list) {
          pw.println("<br>" + link);
        }
      }
    }
    return;
  }

  /**
   * 
   * Query the eavesdrop website to obtain information based on parameters parsed
   * 
   * @return true if data was found successfully else output error message and return false
   *
   */
  private boolean getData(PrintWriter pw, String query_url, String project_value, String year_value)
      throws IOException {
    boolean isSucess = false;

    Document document = Jsoup.connect(OPEN_STACK_URL).get();
    // Invalid value(s)
    if (project_value == null || year_value == null) {
      if (project_value == null) {
        pw.println("Required parameter &lt;project&gt; missing<br>");
      }
      if (year_value == null) {
        pw.println("Required parameter &lt;year&gt; missing<br>");
      }
      return isSucess;
    }

    // Get the project
    Elements links = document.getElementsByAttributeValue("href", project_value + "/");
    if (links != null && !links.isEmpty()) {
      document = Jsoup.connect(links.attr("abs:href")).get();
      // Get the year
      links = document.getElementsByAttributeValue("href", year_value + "/");
      if (links != null && !links.isEmpty()) {
        document = Jsoup.connect(links.attr("abs:href")).get();
        // Get all the data
        links = document.getElementsByAttributeValueStarting("href", project_value);
        // Add URL to history list
        history_list.add(query_url);
        if (links != null && !links.isEmpty()) {
          // Add all data to data list
          for (Element link : links) {
            data_list.add(link.html());
          }
        }
        isSucess = true;
      } else {
        pw.println("Invalid year &lt;" + year_value + "&gt; for project " + project_value + "<br>");
      }
    } else {
      pw.println("Project with &lt;" + project_value + "&gt; not found<br>");
    }

    return isSucess;
  }

}
